var sendTime = 0;
var dataSize = 0;
function connectToServer() {
	var ip = document.getElementById('ip').value;
	var port = document.getElementById('port').value;
	var host = "ws://" + ip + ":" + port;
	try {
		socket = new WebSocket(host);

		socket.onopen = function (openEvent) {
			document.getElementById("serverStatus").innerHTML = 
				'WebSocket Status:: Socket Open';
		};

		socket.onmessage = function (messageEvent) {

			if (messageEvent.data instanceof Blob) {
				var destinationCanvas = document.getElementById('destination');
				var destinationContext = destinationCanvas.getContext('2d');
				var image = new Image();
				image.onload = function () {
					destinationContext.clearRect(0, 0, 
							destinationCanvas.width, destinationCanvas.height);
					destinationContext.drawImage(image, 0, 0);
				}
			image.src = URL.createObjectURL(messageEvent.data);
			} else {
				var split = messageEvent.data.split(":");
				var sendTimePrint = (split[1]-sendTime).toString();
				var receiveTimePrint = (Date.now()-split[1]).toString();
				var roundtripTimePrint = (Date.now()-sendTime).toString();
				var sendTimeSpeed = dataSize/sendTimePrint/1000;
				var receiveTimeSpeed = dataSize/receiveTimePrint/1000;
				var roundtripTimeSpeed = dataSize*2/roundtripTimePrint/1000;
				var timeError = (parseInt(sendTimePrint)+parseInt(receiveTimePrint))-roundtripTimePrint;
				document.getElementById("serverResponse").innerHTML = "Send time: " + sendTimePrint + "ms. Speed: " + sendTimeSpeed 
						+ "MB/s.<br>" + "Receive time: " + receiveTimePrint + "ms. Speed: " + receiveTimeSpeed
						+ "MB/s.<br>Roundtrip time: " + roundtripTimePrint + "ms. Speed: " + roundtripTimeSpeed
						+ "MB/s.<br>Time error: " + timeError + "ms.";
			}
		};

		socket.onerror = function (errorEvent) {
		document.getElementById("serverStatus").innerHTML = 
		  'WebSocket Status:: Error was reported';
		};

		socket.onclose = function (closeEvent) {
		document.getElementById("serverStatus").innerHTML = 
		  'WebSocket Status:: Socket Closed';
		};
	}
	catch (exception) { if (window.console) console.log(exception); }
}

function sendTextMessage() {
	if (socket.readyState != WebSocket.OPEN) return;

	var e = document.getElementById("textmessage").value;
	var data = "";
	for(i=0;i<e;i++){
		data += Math.floor(Math.random() * 9);
	}
	data += ":";
	dataSize = data.length;
	sendTime = Date.now();
	socket.send(data);
}

function sendBinaryMessage() {
	if (socket.readyState != WebSocket.OPEN) return;

	var sourceCanvas = document.getElementById('source');

	socket.send(sourceCanvas.msToBlob());
}    